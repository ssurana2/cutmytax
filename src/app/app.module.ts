import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { MzNavbarModule, MzIconModule, MzIconMdiModule, MzButtonModule, MzSidenavModule } from 'ngx-materialize';
import { CStyleDirective } from './c-style.directive';
import { NavbarComponent } from './navbar/navbar.component';
import { HeaderComponent } from './home/header/header.component';
import { FooterComponent } from './footer/footer.component'

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    CStyleDirective,
    NavbarComponent,
    HeaderComponent,
    FooterComponent
  ],
  imports: [
    BrowserAnimationsModule,
    MzNavbarModule,
    MzIconModule,
    MzIconMdiModule,
    MzButtonModule,
    MzSidenavModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
