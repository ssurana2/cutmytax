import { Component } from '@angular/core';

@Component({
  selector: 'cmt-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'cmt';
}
