import { Directive, ElementRef, AfterViewInit, Input } from '@angular/core';

@Directive({
  selector: '[cmtCStyle]'
})
export class CStyleDirective implements AfterViewInit {

  constructor(private elRef: ElementRef) { }

  @Input() tag: string;
  @Input() value: string;

  ngAfterViewInit() {
    const el: HTMLElement = this.elRef.nativeElement;
    const tag: HTMLElement = el.querySelector(this.tag);
    (this.value.split(" ") || []).forEach(str => tag.classList.add(str));
  }

}
